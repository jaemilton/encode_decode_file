#!/usr/bin/python3
# -*- coding: UTF-8 -*-
import sys
import base64
import binascii

LINE_SIZE = 64
HEADER_FILE = "-----BEGIN CERTIFICATE-----\n"
FOOTER_FILE = "-----END CERTIFICATE-----\n"

def get_input_file_parameter_values(argv, parameter_identifyer):
    if (parameter_identifyer in argv):
        index = argv.index(parameter_identifyer)
        if (len(argv) <= index):
            raise ValueError("Error: parameter value for " + parameter_identifyer + " was not defined.")
        else:
            return argv[index+1:]
    else:
        return None

def encode(in_file, out_file):
    binary_file = open(in_file, "rb") # opening for [r]eading as [b]inary
    
    binary_file_data = binary_file.read()
    base64_encoded_data = base64.b64encode(binary_file_data).decode('utf-8')
    binary_file.close()

    base64_lines = []
    base64_lines.append(HEADER_FILE)
    start_index = 0
    end_index = LINE_SIZE
    while True:
        base64_encoded_data_line = base64_encoded_data[start_index:end_index]
        start_index += LINE_SIZE 
        end_index += LINE_SIZE 
        if (not base64_encoded_data_line):
            break
        base64_lines.append(base64_encoded_data_line + '\n')
    base64_lines.append(FOOTER_FILE)

    base64_file = open(out_file, "wt") # open for [w]riting as [b]inary
    base64_file.writelines(base64_lines)
    base64_file.close()

def decode(in_file, out_file):
    #binary_file = open("/home/jaemilton/Downloads/encode/Fiddler.original.7z", "rb") # opening for [r]eading as [b]inary
    #binary_file_data_teste = binary_file.read()
    #base64_encoded_data_test = base64.b64encode(binary_file_data_teste).decode('utf-8')
    #binary_file.close()



    base64_file = open(in_file, "rt") # opening for [r]eading as [b]inary
    base64_lines = base64_file.readlines() 
    base64_file.close()
    lines_number = len(base64_lines)
    base64_encoded_data = ''
    if ((base64_lines[0] == HEADER_FILE) and (base64_lines[lines_number-1] == FOOTER_FILE)):
        linecount = 0
        for base64_line in base64_lines[1:lines_number-1]:
            linecount += 1
            line_value = base64_line[:LINE_SIZE]
            base64_encoded_data += line_value.replace('\n','') if line_value.endswith('\n') else line_value
    else:
        raise Exception("File without header or foother")    

    #if(base64_encoded_data == base64_encoded_data_test):
    #    pass

    binary_file_data = base64.b64decode(base64_encoded_data.encode('utf-8'))
    #if(binary_file_data_teste == binary_file_data):
    #    pass

    

    base64_file = open(out_file, "wb") # open for [w]riting as [b]inary
    base64_file.write(binary_file_data)
    base64_file.close()
    

def start(argv):
    if (('-h' in argv) or ('-?' in argv)):
        print("""
        python3 encodeutil.py -option InFile OutFile
        Program to Encode file to Base64
        Parameters:
            -decode           -- Decode Base64-encoded file
            -encode           -- Encode file to Base64
        """)
    
    elif ((len(argv) < 4) or  
            (
                not ('-decode' in argv) and
                not ('-encode' in argv)
            )
        ):
        print("Input error. To run, call as python3 encodeutil.py -option InFile OutFile")
    else:

        if ('-decode' in argv):
            files = get_input_file_parameter_values(sys.argv,'-decode')
            decode(in_file = files[0], out_file = files[1])
        else:
            files = get_input_file_parameter_values(sys.argv, '-encode')
            encode(in_file = files[0], out_file = files[1])

start(sys.argv)
